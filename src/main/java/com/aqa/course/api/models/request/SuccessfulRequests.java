package com.aqa.course.api.models.request;

import com.aqa.course.api.models.Customer;
import com.aqa.course.api.models.Items;
import org.apache.http.HttpStatus;

/**
 * SuccessfulRequests contain base API requests with status code verification and data serialization.
 *
 * @author alexpshe
 * @version 1.0
 */
public class SuccessfulRequests {
    private final Requests requests;

    public SuccessfulRequests() {
        this.requests = new Requests();
    }

    public void createCustomer(Customer customer) {
        requests.createCustomer(customer)
                .then()
                .assertThat().statusCode(HttpStatus.SC_CREATED);
    }

    public void createItem(Items item) {
        requests.createItem(item)
                .then()
                .assertThat().statusCode(HttpStatus.SC_OK);
    }

    public Items getItem(String itemId) {
        return requests.getItem(itemId)
                .then()
                .assertThat().statusCode(HttpStatus.SC_OK)
                .extract().body().as(Items.class);
    }

    public Items deleteItem(String itemId) {
        return requests.deleteItem(itemId)
                .then()
                .assertThat().statusCode(HttpStatus.SC_OK)
                .extract().body().as(Items.class);
    }

    public Customer getCustomer(String customerId) {
        return requests.getCustomer(customerId)
                .then()
                .assertThat().statusCode(HttpStatus.SC_OK)
                .extract().body().as(Customer.class);
    }

    public void deleteCustomer(String customerId) {
        requests.deleteCustomer(customerId)
                .then()
                .assertThat().statusCode(HttpStatus.SC_ACCEPTED);
    }
}
